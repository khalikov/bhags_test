import asyncio
import os

import aiohttp

HOST = os.getenv('HOST', '127.0.0.1')
PORT = int(os.getenv('PORT', 8009))

URL = f'http://{HOST}:{PORT}/ws?token=32'


async def main():
    async with aiohttp.ClientSession(
        headers={"Connection": "close"}
    ) as session:
        async with session.ws_connect(URL) as ws:

            # await prompt_and_send(ws)
            while True:
                await asyncio.sleep(1)
                msg = await ws.receive()
                print('Message received from server:', msg)
                if msg.type in (aiohttp.WSMsgType.CLOSED,
                                aiohttp.WSMsgType.ERROR):
                    break


async def prompt_and_send(ws):
    new_msg_to_send = input('Type a message to send to the server: ')
    if new_msg_to_send == 'exit':
        print('Exiting!')
        raise SystemExit(0)
    await ws.send_str(new_msg_to_send)


if __name__ == '__main__':
    print('Type "exit" to quit')
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
