import pika
from pika.credentials import PlainCredentials
credentials = PlainCredentials('guest', 'guest')

params = pika.ConnectionParameters(host='rabbitmq',
         #                          port=port,
         #                          virtual_host=virtual_host,
                                   credentials=credentials,
         #                          frame_max=10000)
)
connection = pika.BlockingConnection(params)
channel = connection.channel()
channel.basic_publish(exchange='test_queue', routing_key='test_queue',
                      body=b'{"id": 32, "msg": "test msg for me"}')
connection.close()

