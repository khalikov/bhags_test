import aiohttp
import logging
import json
from aiohttp import web
from aiohttp.web_exceptions import HTTPFound, HTTPUnauthorized


async def test_handler(request):
    """
    Test handler. Needed to fast connectivity changes detect

    """
    return web.Response(text='OK 800')


async def ws_handler(request):
    """
    WebSocket handler. Needs to receive ws connections,
    Also used as consumer from rabbitmq reattaching asyncio.Queue

    """
    token = request.rel_url.query.get('token')
    logging.debug("token is %s" % token)
    if isinstance(token, str) and len(token) < 100 and token.isdigit():
        ws = web.WebSocketResponse()
        await ws.prepare(request)
        request.app.auth[token] = ws
        while True:
            msg = await request.app.q.get()
            if msg and str(msg['id']) in request.app.auth:
                logging.debug(f'sending to {msg["id"]}'
                await request.app.auth[str(msg['id'])].send_str(msg['msg'])
        return ws
    raise HTTPUnauthorized()
