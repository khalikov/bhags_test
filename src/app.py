import asyncio
import json
import logging
import aio_pika
from aiohttp import web, web_runner

from bhags.handlers import (
    ws_handler, test_handler)

logging.basicConfig(level=logging.DEBUG)
logging.getLogger("asyncio").setLevel(logging.DEBUG)
logging.getLogger('aiohttp').setLevel(logging.DEBUG)



class WebsocketApplication(web.Application):
    """
    q - asyncio.Queue, needs for interchanging between 2 tasks
    auth - dict of websockets authenticated to get messages
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.router.add_get('/ws', ws_handler)
        self.router.add_get('/test', test_handler)
        self.q = asyncio.Queue()
        self.auth = {}

async def main(loop, app):
    connection = await aio_pika.connect_robust(
        "amqp://guest:guest@rabbitmq/", loop=loop
    )

    async with connection:
        queue_name = "test_queue"

        # Creating channel
        channel = await connection.channel()  # type: aio_pika.Channel

        # Declaring queue
        queue = await channel.declare_queue(
            queue_name,
            auto_delete=True
        )  # type: aio_pika.Queue
        
        async with queue.iterator() as queue_iter:
            # Cancel consuming after __aexit__
            async for message in queue_iter:
                async with message.process():
                    logging.error(message.body)  # bytes
                    try:
                        msg = json.loads(message.body.decode())
                    except Exception:
                        logging.error('message is not json')
                    else:
                        if isinstance(msg, dict):
                            _id = msg.get('id')
                            _msg = msg.get('msg')
                            if (
                                isinstance(_id, int) and
                                _id > 0 and isinstance(
                                _msg, str
                            )):
                                await app.q.put({'id': _id, 'msg': _msg})



async def start_http(app):
    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, '0.0.0.0', 8009)
    await site.start()
    #web.run_app(app, port=8009, loop=loop)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    app = WebsocketApplication()
    task1 = loop.create_task(start_http(app))
    task2 = loop.create_task(main(loop, app))
    loop.run_forever()
    
