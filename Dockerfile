FROM python:3.7
MAINTAINER "Ruslan Khalikov"

ADD src/requirements.txt /opt/requirements.txt
RUN pip install -r /opt/requirements.txt

ADD src /opt

WORKDIR /opt
RUN export PYTHONPATH=.

CMD ["python", "/opt/app.py"]